public class Tree {
  private int maxThickness = 100;
  private int branchCount = 0;
  private int age = 0;
  private Branch[] branches;
  public Tree(int size) {
    // Bitshift to get powers of 2.
    // Will probably need to be changed if/when branches stop splitting into 2
    branches = new Branch[(1<<size)];
    branches[0] = new Branch();
    branchCount += 1;
  }

  // Draws all the tree with the base at x, y, z
  public void draw() {
    fill(0,120,0);  // Leaf colour
    stroke(90,0,30);// Branch colour
    // The old code that sorta did the stuff
    for (int branch = 0; branch < branchCount; branch++) {
      if (this.branches[branch].growState == 0) { // While branch is growing
        // Draw the branch
        strokeWeight(this.branches[branch].thickness());
        drawBranch(this.branches[branch]);
        // Draw the leaf
        strokeWeight(0);
        drawLeaf(this.branches[branch]);
        // When the branch is long enough
        if (this.branches[branch].length() >= maxThickness) {
          // Spawn two new branches
          this.branches[branchCount] = new Branch(this.branches[branch]);
          branchCount += 1;
          this.branches[branchCount] = new Branch(this.branches[branch]);
          branchCount += 1;
          // Kill the branch
          this.branches[branch].growState = 1;
        }
      } else if (this.branches[branch].growState == 1) { // Redraw dead branches
        stroke(90,0,30);
        strokeWeight(this.branches[branch].thickness());
        drawBranch(this.branches[branch]);
      }
    }
  }

  // Grows the tree by given increment
  public void grow(int inc) {
    age += inc;
  }

  private class Branch {
    Vector3 origin = new Vector3();
    Vector3 direct = new Vector3();
    float growRate;
    int startAge;
    int growState;  // 0 For fresh, 1 for spliting, 2 for split, 3 dead and not growing
    // The first node
    public Branch() {
      origin.x = 0;
      origin.y = 0;
      origin.z = 0;
      direct.y = -1; // Point up
      growRate = 1;
      startAge = 0;
      growState = 0;
    }
    // Node that inherets parent with deviations
    public Branch(Branch Parent) {
      direct.uniformRandom();
      if (direct.y > 0) {
        direct.y = 0 - direct.y;
      }
      origin.x = Parent.origin.x + (Parent.direct.x*Parent.length());
      origin.y = Parent.origin.y + (Parent.direct.y*Parent.length());
      origin.z = Parent.origin.z + (Parent.direct.z*Parent.length());
      growRate = Parent.growRate*random(0.3,0.9);
      startAge = age;
      growState = 0;
    }
    public float length() {
      float ret = this.growRate*(age-this.startAge);
      if (ret >= maxThickness) {
        return 100;
      } else {
        return ret;
      }
    }
    public float thickness() {
      return (this.growRate*(age-this.startAge)/70.0) + 5;
    }
  }

  // Just some things to clean the code up
  private void drawSphere(float x, float y, float z, float r) {
    translate(x, y, z);
    sphere(r);
    translate(-x, -y, -z);
  }
  private void drawLeaf(Branch branch) {
    drawSphere(
      branch.origin.x + (branch.direct.x*branch.length()),
      branch.origin.y + (branch.direct.y*branch.length()),
      branch.origin.z + (branch.direct.z*branch.length()),
      10.0+(age/100.0)
    );
  }
  private void drawBranch(Branch branch) {
    line(
      branch.origin.x,
      branch.origin.y,
      branch.origin.z,
      branch.origin.x + branch.direct.x*branch.length(),
      branch.origin.y + branch.direct.y*branch.length(),
      branch.origin.z + branch.direct.z*branch.length()
    );
  }
}

// @TODO Probably find some other implimentation of vectors and use that instead of this
public class Vector3 {
  float x;
  float y;
  float z;

  public Vector3() {
    x = 0;
    y = 0;
    z = 0;
  }

  // Creates a unit vector with uniform a distribution
  public void uniformRandom() {
    // @TODO Check this math, its probs wrong
    float t = PI*random(0,2);
    this.z = random(-1,1);
    this.x = sqrt(1-(z*z))*cos(t);
    this.y = sqrt(1-(z*z))*sin(t);
  }
}
