// Create the tree
Tree tree = new Tree(8);

void setup() {
  size(1600, 900, P3D);
  lights();
  directionalLight(255, 255, 255, 0, 0, -1);
  ambientLight(255, 255, 255);
  frameRate(59);
}

void draw() {
  lights();
  background(70,150,200); // Clear last frame
  camera(mouseX, height/2, (height/2) / tan(PI/6), width/2, height/2, 0, 0, 1, 0);

  // Draw tree at location
  translate(width/2, height, width/-3);
  tree.draw();
  translate(-width/2, -height, -width/-3);

  // Grow the tree by 1 unit
  tree.grow(1);
}
